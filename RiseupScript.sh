#!/bin/bash
## Welcome to Envy's Riseup.net OpenVPN file creator.
## This bash script simply create a .ovpn file capable of connecting to the Riseup Red VPN network.
## You must have a riseup.net account to use the VPN.
## satanism@riseup.net
## twitter.com/proanarchism
## github.com/9-5
## The project can be found at https://github.com/9-5/Riseup-VPN-Service/

create () {
    echo "Creating riseup.ovpn..."
    touch riseup.ovpn
    echo "Adding all necessary features."
    echo "client" > ./riseup.ovpn
    echo "cipher AES-256-CBC" > ./riseup.ovpn
    echo "auth SHA256" > ./riseup.ovpn
    echo "remote vpn.riseup.net 1194" > ./riseup.ovpn
    echo "#Alternate Ports: (Remove the '#' from them.)" > ./riseup.ovpn
    echo "#remote vpn.riseup.net 443" > ./riseup.ovpn
    echo "#remote vpn.riseup.net 80" > ./riseup.ovpn
    echo "dev tun" > ./riseup.ovpn
    echo "#Type of packets:" > ./riseup.ovpn
    echo "proto udp" > ./riseup.ovpn
    echo "#proto tcp" > ./riseup.ovpn
    echo "auth-user-pass" > ./riseup.ovpn
    echo "Adding Riseup certificate..."
    echo "<ca>" > ./riseup.ovpn
    echo "-----BEGIN CERTIFICATE-----" > ./riseup.ovpn
    echo "MIIF2jC
CA8KgAwIBAgIIVogyQTSIzc8wDQYJKoZIhvcNAQELBQAwgYYxGDAWBgNV" > ./riseup.ovpn
    echo "BAMTD1Jpc2V1cCBOZXR3b3JrczEYMBYGA1UEChMPUmlzZXVwIE5ldHdvcmtzMRAw" > ./riseup.ovpn
    echo "DgYDVQQHEwdTZWF0dGxlMQswCQYDVQQIEwJXQTELMAkGA1U``EBhMCVVMxJDAiBgkq" > ./riseup.ovpn
    echo "hkiG9w0BCQEWFWNvbGxlY3RpdmVAcmlzZXVwLm5ldDAiGA8yMDE2MDEwMjIwMjU0" > ./riseup.ovpn
    echo "MFoYDzIwMjYwMzMwMjAyNjAxWjCBhjEYMBYGA1UEAxMPUmlzZXVwIE5ldHdvcmtz" > ./riseup.ovpn
    echo "MRgwFgYDVQQKEw9SaXNldXAgTmV0d29ya3MxEDAOBgNVBAcTB1NlYXR0bGUxCzAJ" > ./riseup.ovpn
    echo "BgNVBAgTAldBMQswCQYDVQQGEwJVUzEkMCIGCSqGSIb3DQEJARYVY29sbGVjdGl2" > ./riseup.ovpn
    echo "ZUByaXNldXAubmV0MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAw2VV" > ./riseup.ovpn
    echo "uoz4xqeB1ROIwXBRaj0prOqEFX89A7+2rslGRfjM8NPHyBLGleoHTK3DPwadtQeg" > ./riseup.ovpn
    echo "ulaEOAjM5EMXTEX/o9H46L6h729HUWPCwVssvvOjyxTyGJDf7Ihd/Ab7ODtlJSyc" > ./riseup.ovpn
    echo "g31aXMioA5pGz5QnS3VGz4nE9+NL+jobc/NbhaacsEPR/7xO7meRNu/1S+YiHK1y" > ./riseup.ovpn
    echo "BSVrfap3XItlcNHDGNQkPyyJbS3pAS1lQs2HCBTzcFCamCkDOC7cRh9wZ4GH8U2f" > ./riseup.ovpn
    echo "2s0mDD5zhRpheNW4gFBtGpqHiRXv7WJW612aaXzKQQoIq2loGNvOpnyBPKL3jjUT" > ./riseup.ovpn
    echo "Rxv5IzWMV0nAofMCy25u/S4J65uSEd9mLNXFJ3rl+cFaybcOUXktTbS7bZy6cMyf" > ./riseup.ovpn
    echo "/gO28bEXIWr5WfZf8jCbPyOVfExZquG3aS+0YPWmIJCheXQzgiwplZy93oND1GGQ" > ./riseup.ovpn
    echo "f+1R2F7GPwNXQdefv2xm7PTWhHbSWHHmeY89qYED+yFJrX5ChoFoBbYs1lMmdU/C" > ./riseup.ovpn
    echo "2MnQBFtvcVockXFAUONyMKiq8ZP6sQ1lu0rO9Bvkhx55sJLZOmjN3g4S1K97PbbI" > ./riseup.ovpn
    echo "5DzHKcR0JQSt8ZtCY/MuMbwvlNYo98bFWvlfKET0KPtogNNH0PNfJmStKR8jWGjE" > ./riseup.ovpn
    echo "HnUNXo7YDfK90iEKTjLz2K5CYzH5Dm6iYJNaaykCAwEAAaNGMEQwEgYDVR0TAQH" > ./riseup.ovpn
    echo "BAgwBgEB/wIBADAPBgNVHQ8BAf8EBQMDBwQAMB0GA1UdDgQWBBTGek7ebtq2Ibm+" > ./riseup.ovpn
    echo "2K6je1IMobvEkzANBgkqhkiG9w0BAQsFAAOCAgEAO2B3jnL+8LeoRkc282qUpHyu" > ./riseup.ovpn
    echo "xYj0Qd68l0CJ0FjfA2OCR/6h1W4gZVH+fTd/mhgrNXj28GRT53JEh1jdRC7ENTXu" > ./riseup.ovpn
    echo "W9O8I9gCbWQ6V4nkZ9lpq8UEmKTFGnngVu8VCmSDF+y0kFuEtmt0jyd2UkJfC/vy" > ./riseup.ovpn
    echo "Gh78OCHEdGAeOTYHXamiuA9Z7wMuncPjP476gSW2kfWTdxV25ad4tT5dA5d42xDm" > ./riseup.ovpn
    echo "YE2UKzHeB9amOmvyh08LPD0idT5oROCIHsHBhQC9oltJXO5j6GyHRg88C1inyv6R" > ./riseup.ovpn
    echo "xk+w9ek4wSBpoJg5t3hdbZr3JTUsuu4WPtAET0fMQpJC+niaBbegwtvdLZFM+d8x" > ./riseup.ovpn
    echo "ead3ZpMO+XrpazDFGtdPTQdi5EIYmr2RL9eTeQbVPwMB9TgFpBXP+iYIuTpNo8jn" > ./riseup.ovpn
    echo "8zS4EcPRmz6PQJVK4zkHczfvquyU9RuOwEgb8qN4tSNxF0Z94uSVUoXCG9WZLf8q" > ./riseup.ovpn
    echo "MfsGesYiR/qLnLn3MfAyWm3OVOUvGzczDE2T8VvY7rXc2+8ra5aK0TNAgEz9ey6D" > ./riseup.ovpn
    echo "/dGzM1JCCe1A08s+2+eRX//pmqmOCoGrY7zwIVS2T249h6iIMM9yT0C3ZXRoTnVN" > ./riseup.ovpn
    echo "osyidOkVuQr0YK6shJ0WaK4F1MktdjOZKPoIc9QLw+TrSU2hfyla36T0bNWMC/TJ" > ./riseup.ovpn
    echo "YtxDI+d1jIFZ7zMmts4=" > ./riseup.ovpn
    echo "-----END CERTIFICATE-----" > ./riseup.ovpn
    echo "</ca>" > ./riseup.ovpn
    echo "nobind" > ./riseup.ovpn
    echo "persist-tun" > ./riseup.ovpn
    echo "persist-key" > ./riseup.ovpn
    echo "resolv-retry infinite" > ./riseup.ovpn
    echo "remote-cert-tls server" > ./riseup.ovpn
    echo "script-security 2" > ./riseup.ovpn
    echo "up /etc/openvpn/update-resolv-conf" > ./riseup.ovpn
    echo "down /etc/openvpn/update-resolv-conf" > ./riseup.ovpn
    echo "Riseup.ovpn has been created."
    echo "Press any key to exit..."
    read junk
    exit
}

dl () {
    echo "Downloading riseup.ovpn from Riseup.net..."
    wget https://riseup.net/vpn/vpn-red/riseup.ovpn
    echo "Finished downloading riseup.ovpn."
    echo "Press any key to exit..."
    read junk
    exit
}

while true; do
    read -p "How would you like to get riseup.ovpn? (1 = Creating the file manually. | 2 = Downloading it directly from Riseup.net | 3 = Exit): " ab
    case $ab in
        [1]* ) create; break;;
        [2]* ) dl;;
        [3]* ) exit;;
        * ) echo "Please only enter one of the following options: 1, 2 or 3.";;
    esac
done
