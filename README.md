# Riseup-VPN-Service

## What is this?

This is just a simple bash script to get the a working .ovpn file for Riseup Red.

## Do I need a Riseup account?

Yes. You need a Riseup Red account to be able to sign into the VPN.

## How can I get a Riseup account?

You'll need an invite code generated from an already existing member. The sign up can be found [here](https://account.riseup.net/user/new)

## Can you generate me an invite code?
No.

## How can I contact the developer of this script?

You can reach me at the following:
```
satanism@riseup.net
twitter.com/proanarchism
github.com/9-5
```
## Credits

All rights to Riseup, Riseup Red and its services are owned by the [Riseup Collective](https://riseup.net/about-us).

This project can be freely modified. I would appreciate accrediation if possible <3.
